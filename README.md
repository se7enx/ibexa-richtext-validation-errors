# Ibexa RichText Validation Errors

This package provides detailed information about RichText validation errors.

## Installation

1. Require via composer
    ```bash
    composer require contextualcode/ibexa-richtext-validation-errors
    ```

2. Clear browser caches and enjoy!

## Usage

Common error message are shown whenever content with invalid RichText fields is edited:

![old-common-error](https://gitlab.com/contextualcode/ibexa-richtext-validation-errors/raw/master/doc/images/old-common-error.png)

Unfortunately, the errors shown are not particularly useful for finding the root causes of the problem!

After this package is installed, errors are much more informative:

![new-detailed-error](https://gitlab.com/contextualcode/ibexa-richtext-validation-errors/raw/master/doc/images/new-detailed-error.png)

Also, we strongly suggest using [contextualcode/ezplatform-alloyeditor-source](https://packagist.org/packages/contextualcode/ezplatform-alloyeditor-source) in addition to this package.
