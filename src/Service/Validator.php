<?php

namespace ContextualCode\IbexaRichTextValidationErrors\Service;

use DOMDocument;
use DOMNodeList;
use EzSystems\EzPlatformRichText\eZ\RichText\Validator\Validator as Base;

class Validator extends Base
{
    private const MAX_DEPTH = 5;

    public function validateDocument(DOMDocument $document): array
    {
        $errors = parent::validateDocument($document);
        if (0 === count($errors)) {
            return $errors;
        }

        $info = $this->getInvalidElementInfo($document);
        if (null !== $info) {
            $errors = [sprintf(
            'Invalid element %s element with "%s" XPath selector: %s',
                $info['title'],
                $info['XPath'],
                $info['element']->ownerDocument->saveXML($info['element'])
            )];
        }

        return $errors;
    }

    private function getInvalidElementInfo(DOMDocument $document): ?array
    {
        $testDom = new DOMDocument();
        $testDom->loadXML($document->saveXML());

        $rootNodes = $testDom->documentElement->childNodes;
        return $this->fixInvalidElement($testDom, $rootNodes);
    }

    private function fixInvalidElement(
        DOMDocument $dom,
        DOMNodeList $nodes,
        int $depth = 1
    ): ?array {
        for ($k = $nodes->length; --$k >= 0;) {
            $node = $nodes->item($k);

            if ($node->childNodes->length > 1 && $depth < self::MAX_DEPTH) {
                $newDepth = $depth+1;
                $info = $this->fixInvalidElement($dom, $node->childNodes, $newDepth);
                if ($info !== null) {
                    return $info;
                }
            }

            $path = $node->getNodePath();
            $node->parentNode->removeChild($node);
            $errors = parent::validateDocument($dom);

            if (count($errors) === 0) {
                $title = '<' . $node->nodeName;
                if (isset($node->attributes)) {
                    foreach ($node->attributes as $attr) {
                        $title .= ' ' . $attr->name . '="' . $attr->value . '"';
                    }
                }
                $title .= '>';

                return [
                    'XPath' => $path,
                    'title' => $title,
                    'depth' => $depth,
                    'element' => $node,
                ];
            }
        }

        return null;
    }
}